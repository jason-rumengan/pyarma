---
title: 'PyArmadillo: a streamlined linear algebra library for Python'
tags:
    - linear algebra
    - scientific computing
    - mathematics
    - Python
authors:
    - name: Jason Rumengan
      orcid: 0000-0003-1839-5138
      affiliation: "1, 2"
    - name: Terry Yue Zhuo
      orcid: 0000-0002-5760-5188
      affiliation: 3
    - name: Conrad Sanderson
      orcid: 0000-0002-0049-4501
      affiliation: "1, 4"
affiliations:
    - name: Data61/CSIRO, Australia
      index: 1
    - name: Queensland University of Technology, Australia
      index: 2
    - name: University of New South Wales, Australia
      index: 3
    - name: Griffith University, Australia
      index: 4
date: 12 October 2021
bibliography: paper.bib
---


# Summary

PyArmadillo is a linear algebra library for the Python language,
with the aim of closely mirroring the programming interface of the widely used Armadillo C++ library, 
which in turn is deliberately similar to Matlab.
PyArmadillo hence facilitates algorithm prototyping with Matlab-like syntax directly in Python,
and relatively straightforward conversion of PyArmadillo-based Python code
into performant Armadillo-based C++ code.
The converted code can be used for purposes such as speeding up Python-based programs
in conjunction with pybind11 [@pybind11],
or the integration of algorithms originally prototyped in Python into larger C++ codebases.

PyArmadillo provides objects for matrices and cubes,
as well as over 200 associated functions for manipulating data stored in the objects.
Integer, floating point and complex numbers are supported.
Various matrix factorisations are provided through integration with LAPACK [@anderson1999lapack],
or one of its high performance drop-in replacements such as Intel MKL [@Intel_MKL] or OpenBLAS [@Xianyi_OpenBLAS].


# Statement of Need

Armadillo is a popular linear algebra and scientific computing library for the C++ language [@Sanderson_2016; @Sanderson_2018]
that has three main characteristics:
(i) a high-level programming interface deliberately similar to Matlab,
(ii) an expression evaluator (based on template meta-programming)
that automatically combines several operations to increase speed and efficiency,
and
(iii) an efficient mapper between mathematical expressions and low-level BLAS/LAPACK functions [@Psarras_2021].
Matlab is widely used in both industrial and academic contexts,
providing a programming interface that allows mathematical expressions to be written in a concise and natural manner [@Linge_MatlabOctave_2016], 
especially in comparison to directly using low-level libraries such as LAPACK [@anderson1999lapack].
In industrial settings, algorithms are often first prototyped in Matlab,
before conversion into another language, such as C++, for the purpose of integration into products.
The similarity of the programming interfaces between Armadillo and Matlab
facilitates direct prototyping in C++,
as well as the conversion of research code into production environments.
Armadillo is also often used for implementing performance critical parts of software packages
running under the R environment for statistical computing [@R_manual], 
via the RcppArmadillo bridge [@Eddelbuettel_2014].

Over the past few years, Python has become popular for data science and machine learning.
This partly stems from a rich ecosystem of supporting frameworks and packages,
as well as lack of licensing costs in comparison to Matlab.
Python allows relatively quick prototyping of algorithms,
aided by its dynamically typed nature
and the interpreted execution of user code,
avoiding time-consuming compilation into machine code.
However, for the joint purpose of algorithm prototyping and deployment,
the flexibility of Python comes with two main issues:
(i) slow execution speed due to the interpreted nature of the language,
(ii) difficulty with integration of code written in Python into larger programs and/or frameworks written in another language.
The first issue can be somewhat addressed through conversion of Python-based code into the low-level Cython language [@Cython].
However, since Cython is closely tied with Python, 
conversion of Python code into C++ may be preferred as it also addresses the second issue,
as well as providing a higher-level of abstraction.

PyArmadillo is aimed at:
(i) users that prefer compact Matlab-like syntax rather than the somewhat more verbose syntax provided by NumPy/SciPy [@harris2020array; @2020SciPy-NMeth],
and
(ii) users that would like a straightforward conversion path to performant C++ code.
More specifically,
PyArmadillo aims to closely mirror the programming interface of the Armadillo library,
thereby facilitating the prototyping of algorithms with Matlab-like syntax directly in Python.
Furthermore, PyArmadillo-based Python code can be easily converted
into high-performance Armadillo-based C++ code.
Due to the similarity of the programming interfaces,
the risk of introducing bugs in the conversion process is considerably reduced.
Moreover, conversion into C++ based code allows taking advantage of expression optimisation
performed at compile-time by Armadillo, resulting in further speedups.
The resulting code can be used in larger C++ programs, 
or used as a replacement of performance critical parts within a Python program
with the aid of the pybind11 interface layer [@pybind11].



# Functionality

PyArmadillo provides matrix objects for several distinct element types: integers, single- and double-precision floating point numbers, as well as complex numbers.
In addition to matrices, PyArmadillo also has support for cubes (3 dimensional arrays), where each cube can be treated as an ordered set of matrices.
Multi-dimensional arrays beyond 3 dimensions are explicitly beyond the scope of PyArmadillo.
Over 200 functions are provided for manipulating data stored in the objects, covering the following areas:
fundamental arithmetic operations, 
contiguous and non-contiguous submatrix views,
diagonal views, 
element-wise functions,
scalar/vector/matrix valued functions of matrices,
generation of various vectors/matrices,
statistics,
signal processing,
storage of matrices in files,
matrix decompositions/factorisations,
matrix inverses,
and equation solvers.
See the online documentation at [https://pyarma.sourceforge.io/docs.html](https://pyarma.sourceforge.io/docs.html) for details.
PyArmadillo matrices and cubes are convertible to/from NumPy arrays,
allowing users to tap into the wider Python data science ecosystem,
including plotting tools such as Matplotlib [@Hunter2007].

# Implementation

PyArmadillo relies on pybind11 [@pybind11] for interfacing C++ and Python,
as well as on Armadillo for the underlying C++ implementation of matrix objects and associated functions.
Due to its expressiveness and relatively straightforward use,
pybind11 was selected over other interfacing approaches such as Boost.Python [@osti_815409] and manually writing C++ extensions for Python.
In turn, Armadillo interfaces with low-level routines in BLAS and LAPACK [@anderson1999lapack],
where BLAS is used for matrix multiplication,
and LAPACK is used for various matrix decompositions/factorisations and equation solvers.
As the low-level routines in BLAS and LAPACK are considered as the _de facto_ standard for numerical linear algebra,
it is possible to use high performance drop-in replacements such as Intel MKL [@Intel_MKL] and OpenBLAS [@Xianyi_OpenBLAS].

PyArmadillo is open-source software, distributed under the Apache 2.0 license [@apache],
making it useful in both open-source and proprietary (closed-source) contexts [@Laurent_2008].
It can be obtained at [https://pyarma.sourceforge.io](https://pyarma.sourceforge.io)
or via the Python Package Index in precompiled form.


# Acknowledgements

We would like to thank our colleagues at Data61/CSIRO
(Dan Pagendam, Dan Gladish, Andrew Bolt, Piotr Szul)
for providing feedback and testing.


# References
